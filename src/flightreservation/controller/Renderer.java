/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightreservation.controller;

import java.awt.Component;
import java.awt.*;
import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.*;

/**
 *
 * @author taohuh
 */
public class Renderer extends DefaultListCellRenderer implements 
        ListCellRenderer<Object>{
    public Component getListCellRendererComponent(JList list, MyFlightDetail value,
            int index, boolean isSelected, boolean cellHasFocus) {

        //this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        //this.removeAll();
        MyFlightDetail flight = (MyFlightDetail) value;
        setText(flight.getAirlineName());
        //setIcon(flight.getImg());
        
        setEnabled(true);
        setFont(list.getFont());
        /*
        double price = value.getPrice();
        JLabel priceLabel = new JLabel(String.valueOf(price));
        priceLabel.setFont(new Font("Courier New", Font.BOLD, 20));
        priceLabel.setForeground(Color.BLUE);
        priceLabel.setPreferredSize(new Dimension(100, 20));
        priceLabel.setMinimumSize(new Dimension(100, 20));
        priceLabel.setMaximumSize(new Dimension(100, 20));
        
        this.add(priceLabel);

        Component rigidArea1
                = Box.createRigidArea(new Dimension(20, 20));
        this.add(rigidArea1);
        
        String airline = value.getAirlineName();
        System.out.println(airline);
        ImageIcon icon = new ImageIcon("src\\resources\\" +airline+"thai.png");
        JLabel airlineLabel = new JLabel(airline, icon, SwingConstants.LEADING);
        this.add(airlineLabel);
                */
        return this;

    }
}
