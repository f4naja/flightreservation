/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightreservation.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author taohuh
 */
public class MyFormResult extends JFrame{
    private JPanel panel1;
    private JPanel panel2;
    public static void main(String[] args){
        EventQueue.invokeLater(new Runnable(){
            @Override
            public void run() {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                MyFormResult formResult = new MyFormResult();
                formResult.setVisible(true);
            }
        
        });
    }
    
    public MyFormResult(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800,450);
        setLocation(500, 280);
        //getContentPane().setLayout(null);
        
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        getContentPane().add(mainPanel);
        
        itemTabPanel1();
        JTabbedPane tabPane = new JTabbedPane();
        tabPane.addTab("ListView",panel1);
        tabPane.addTab("MatrixView",panel2);
        mainPanel.add(tabPane);
       
    }        

    private void itemTabPanel1() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        panel1 = new JPanel();
        panel1.setLayout(null);
        //ScrollPane
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(14,14,700,89);
        panel1.add(scrollPane);
        
        //Table
        JTable table = new JTable();
        scrollPane.setViewportView(table);
        
        //Model for Table
        DefaultTableModel model = (DefaultTableModel)table.getModel();
        model.addColumn("Price");
        model.addColumn("Airline");
        model.addColumn("Take-off");
        model.addColumn("Landing");
        
        
        Connection connect = null;
        //Statement s = null;
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/flight", "root", null);
            //s = connect.createStatement();
           
            PreparedStatement ps;
            ps = connect.prepareStatement("SELECT * FROM flightDetail");
            ResultSet result = ps.executeQuery();
            
            int row = 0;
            while((result!=null) && (result.next())){
                model.addRow(new Object[0]);
                model.setValueAt(result.getDouble("price"),row,0);
                model.setValueAt(result.getString("airportName"), row, 1);
                model.setValueAt(result.getString("Take-off"), row, 2);
                model.setValueAt(result.getString("Landing"), row, 3);
                row++;
            }
            result.close();
            ps.close();
        }catch (Exception e) {
            //Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, e.getMessage());
            e.printStackTrace();
        }
    }
}
